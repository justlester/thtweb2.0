<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');
Route::get('/solutions/audio','PublicController@conAudio');
Route::get('/solutions/web','PublicController@conWeb');
Route::get('/solutions/international','PublicController@conInt');
Route::get('/about-us','PublicController@about');
// Route::get('/careers','PublicController@careers');
Route::get('/faq','PublicController@faq');
Route::get('/company/news','PublicController@news');
Route::get('/company/newsletters','PublicController@newsletters');
Route::get('/company/resources','PublicController@resources');
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
