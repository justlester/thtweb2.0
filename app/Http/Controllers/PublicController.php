<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    //
    public function index(){
        return view('index');
    }

    public function about(){
        return view('about');
    }

    public function conAudio(){
        return view('con_audio');
    }

    public function conWeb(){
        return view('con_web');
    }

    public function conInt(){
        return view('con_int');
    }

    public function careers(){
        return view('careers');
    }

    public function faq(){
        return view('faq');
    }

    public function news(){
        return view('news');
    }

    public function newsletters(){
        return view('newsletters');
    }

    public function resources(){
        return view('resources');
    }
}
