const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/index_style.scss','public/css')
   .sass('resources/sass/con_audio_style.scss','public/css')
   .sass('resources/sass/con_web_style.scss','public/css')
   .sass('resources/sass/con_int_style.scss','public/css')
   .sass('resources/sass/about_style.scss','public/css')
   .sass('resources/sass/careers_style.scss','public/css')
   .sass('resources/sass/faq_style.scss','public/css')
   .sass('resources/sass/news_style.scss','public/css')
   .sass('resources/sass/newsletters_style.scss','public/css')
   .sass('resources/sass/resources_style.scss','public/css');
