$(document).ready(function(){
    var desktop_min_width = 1040;
    
    $('.navbar .navbar-toggle').click(function(event){
        event.preventDefault();
        if( $('.navbar .navbar-menu').is(":visible"))
        {
            $('.navbar .navbar-menu').slideUp('fast',function(){
                $('.navbar-toggle').removeClass('open');
            });
        }
        else
        {
            $('.navbar .navbar-menu').slideDown('fast',function(){
                $('.navbar-toggle').addClass('open');
            });
        }
      
    });

    $(".navbar-menu-item-dropdown a[ data-toggle='dropdown']").click(function(){
        if(getScreenWidth() < desktop_min_width)
        {
            var dropdown_content = $(this).attr('data-toggle-content');
            $(dropdown_content).slideToggle('fast');
        }
    });

    $(".navbar-menu-item-dropdown[ data-hover='dropdown']").hover(function(){
        if(getScreenWidth() >= desktop_min_width)
        {
            var hover_content = $(this).attr('data-hover-content');
            $(hover_content).slideToggle('fast');
        }
    });

    $("a[data-scroll-location='true']").on('click', function(event) {
        if (this.hash !== "") {
          event.preventDefault();
          var hash = this.hash;
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
            window.location.hash = hash;
          });
        } 
    });

    $(window).resize(function(){
        $(".dropdown-content").hide();
    });

    function getScreenWidth(){
        return window.innerWidth;
    }
});