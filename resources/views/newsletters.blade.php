@extends('layouts.layout')


@section('title')
Partners
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/newsletters_style.css')}}">
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h1>Newsletters</h1>
                <div class="newsletters-list row">
                    <div class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050418.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Apr. 18, 2005</a>
                    </div>                    
                    <div class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050404.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Apr. 4, 2005</a>
                    </div>
                    <div class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050307.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Mar. 7, 2005</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050222.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Feb. 22, 2005</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050207.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Feb. 7, 2005</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050124.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Jan. 24, 2005</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20050110.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Jan. 10, 2005</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20041213.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Dec. 13, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20041102.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Nov. 2, 2004</a>
                    </div>
      
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20041005.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Oct. 5, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040921.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Sept. 21, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040830.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Aug. 30, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040816.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Aug. 16, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040802.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Aug. 2, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040726.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Jul. 26, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040712.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Jul. 12, 2004</a>
                    </div>
                    <div  class="newsletter-item col-6 col-sm-6 col-md-3">
                        <a href="{{asset('files/newsletters/20040624.html')}}" target="_blank"><i class="far fa-newspaper"></i>&nbsp;Jun. 24, 2004</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection