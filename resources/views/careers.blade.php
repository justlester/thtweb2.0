@extends('layouts.layout')


@section('title')
Careers
@endsection

@section('styles')
<link rel="stylesheet" href="{{mix('css/careers_style.css')}}">
@endsection


@section('content')
<div class="section-1">
    <div class="container">
        <div class="card join-the-team">
            <div class="card-body">
                <h1>
                    Join the Team
                </h1>
                <p>THT Web Worldwide is considering qualified applicants for the following positions. <br/> If interested, please email your resume to 
                </p>
                <a class="btn mail-to" href="mailto:jobs@thtweb.com">jobs@thtweb.com</a>
            </div>
        </div>
        <div class="row avail-positions-container">
            <div class="col-lg-6 position-item">
                <div class="card">
                    <div class="card-position-title">
                        <div class="inner-position-title">
                            Inside Sales 
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            These individuals will identify and close business accounts over the telephone. High phone activity, aggressive and competitive spirit and the ability to learn quickly are all key qualities for success. They will be responsible for generating new sales revenue through cold calling and signing on new customers over the phone. 
                        </p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Qualifications</h5>
                        <p class="card-text">College degree preferred. We are looking for a positive, enthusiastic personality with a strong desire to be successful. Excellent verbal and written skills are a must with a working knowledge of Microsoft Word, Excel, PowerPoint, Outlook and Internet Explorer.</p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Benefits</h5>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Medical, Dental & Vision Insurance</li>
                            <li class="list-group-item">Life & Disability Insurance</li>
                            <li class="list-group-item">Flexible Spending Plan</li>
                            <li class="list-group-item">401(k) Plan</li>
                            <li class="list-group-item">Holidays, Vacation & Sick Days</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 position-item">
                <div class="card">
                    <div class="card-position-title">
                        <div class="inner-position-title">
                            Major Account Managers  
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            As a Major Account Manager, the focus is on business-to-business selling to new clients and existing clients. New revenue generation is key though consultative type selling to mid to larger business customer. The ideal candidate must be a highly motivated self-starter who is results-oriented with the ability to work independently as well as within a team. 
                        </p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Qualifications</h5>
                        <p class="card-text">College degree or equivalent work experience and three plus years of Telecommunications Sales selling to mid to large businesses is required. We are looking for a positive, enthusiastic personality with a strong desire to be successful. Excellent verbal and written skills are a must with a working knowledge of Microsoft Word, Excel, PowerPoint, Outlook and Internet Explorer. </p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Benefits</h5>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Medical, Dental & Vision Insurance</li>
                            <li class="list-group-item">Life & Disability Insurance</li>
                            <li class="list-group-item">Flexible Spending Plan</li>
                            <li class="list-group-item">401(k) Plan</li>
                            <li class="list-group-item">Holidays, Vacation & Sick Days</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection