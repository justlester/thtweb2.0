<div class="navbar">
    <div class="navbar-container">
        <div class="navbar-brand">
            <a href="{{url('/')}}">
                <img src="{{asset('images/thtweb_logo.png')}}" alt="thtweb_logo">
            </a>
        </div>
        <div class="navbar-menu">
            <div class="navbar-menu-item {{ Request::path() == '/' ? 'active' : '' }}">
                <a href="{{url('/')}}">
                    Home
                    <div class="underline-hover"></div>
                </a>
            </div>
            <div class="navbar-menu-item-dropdown 
                {{ Request::path() == 'solutions/audio'
                    ||  Request::path() == 'solutions/web'
                        || Request::path() == 'solutions/international'
                            ? 'active' : '' }}" data-hover='dropdown' data-hover-content="#conferencing" >
                <a data-toggle="dropdown" data-toggle-content="#conferencing" href="#">
                    Solutions&nbsp;<i class="fas fa-sort-down"></i>
                    <div class="underline-hover"></div>
                </a>
                <div id="conferencing" class="dropdown-content">
                    <ul>
                        <li>
                            <a href="{{url('/solutions/audio')}}">
                                Audio Conferencing
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/solutions/web')}}">
                                Web Conferencing
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/solutions/international')}}">
                                International Conferencing
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="navbar-menu-item {{ Request::path() == 'about-us' ? 'active' : '' }}">
                <a href="{{url('/about-us')}}">
                    About Us
                    <div class="underline-hover"></div>
                </a>
            </div>
            {{-- <div class="navbar-menu-item {{ Request::path() == 'careers' ? 'active' : '' }}">
                <a href="{{url('/careers')}}">
                    Careers
                    <div class="underline-hover"></div>
                </a>
            </div> --}}
            <div class="navbar-menu-item {{ Request::path() == 'faq' ? 'active' : '' }}">
                <a href="{{url('/faq')}}">
                    FAQ
                    <div class="underline-hover"></div>
                </a>
            </div>
            <div class="navbar-menu-item-dropdown 
                {{ Request::path() == 'company/news'
                    ||  Request::path() == 'company/newsletters'
                        || Request::path() == 'company/resources'
                            ? 'active' : '' }}" data-hover='dropdown' data-hover-content="#company" >
                <a data-toggle="dropdown" data-toggle-content="#company" href="#">
                    Company&nbsp;<i class="fas fa-sort-down"></i>
                    <div class="underline-hover"></div>
                </a>
                <div id="company" class="dropdown-content">
                    <ul>
                        <li>
                            <a href="{{url('/company/news')}}">
                                News        
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/company/newsletters')}}">
                                Newsletters
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/company/resources')}}">
                                Resources
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="navbar-menu-item-sign-up">
                <a href="#" data-toggle="modal" data-target="#signup_modal">
                    Sign Up&nbsp;<i class="fas fa-sign-in-alt"></i>
                </a>
            </div>
        </div>
    </div>
    
    <div class="navbar-mobile-right">
        <a href="#" data-toggle="modal" data-target="#signup_modal" class="sign-up-mobile">
            Sign Up&nbsp;<i class="fas fa-sign-in-alt"></i>
        </a>
        {{-- <a href="#" class="navbar-toggle">
            <i class="fas fa-bars"></i>
            <i class="fas fa-times" style="display:none;"></i>
        </a> --}}
        <a href="#" class="navbar-toggle">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </a>
    </div>
</div>