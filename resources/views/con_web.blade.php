@extends('layouts.layout')


@section('title')
Web Conferencing
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/con_web_style.css')}}">
@endsection

@section('content')
    <div class="section-1" id="main_selection">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h1><b>Web Conferencing</b></h1>
                    <p class="text-center">
                        Our web solutions add visuals, collaboration and interaction to any type of audio conference. They are powerful tools to communicate with the Board of Directors, direct reports, clients - anyone who needs to see and hear your message.  
                    </p>
                    <p class="text-center">
                        Web conferencing requires minimal effort. You control the presentation from your desktop while your audience accesses it via the web. You can present a slide show, collaborate on documents, share applications, poll your audience and more. 
                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <a class="scroll-links" data-scroll-location="true" href="#conference_complete">
                                <div class="inner-scroll-links">
                                    Conference Complete
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a class="scroll-links" data-scroll-location="true" href="#features">
                                <div class="inner-scroll-links">
                                    Features
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a class="scroll-links" data-scroll-location="true" href="#streaming_media">
                                <div class="inner-scroll-links">
                                    Streaming Media
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <a class="scroll-links" data-scroll-location="true" href="{{url('/solutions/international')}}">
                                <div class="inner-scroll-links">
                                    International Web Conferencing
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-2" id="conference_complete">
        <div class="container">
            <a class="back-to-selection" href="#main_selection" data-scroll-location="true">Back to Top&nbsp;<i class="fas fa-arrow-up"></i></a>
            <h1 class="text-center">Conference Complete</h1>
            <p class="text-justify">
                While there are many applications available for collaborative work on the web, our experience shows that most web conference users simply want to share static content. 
            </p>
            <p class="text-justify">
                Understanding this, THT Web Worldwide has bundled together a product we call <b>Conference Complete</b> that allows you to upload your documents and share them over the web while on your audio conference call. THT Web Worldwide offers this at <b>no additional cost</b>! 
            </p>
            <p class="text-justify">
                Conference Complete offers:
            </p>
            <ul>
                    <li> Audio connections for up to 100 users without a reservation
                    </li><li> Simultaneous web conferencing
                    </li><li> Ability to record your synchronized audio and visual playback
                    </li><li> Ability to stream your call over the web in near-real time, using Real Player&trade; (great for <a href="{{url('/solutions/international')}}">international users</a>)
                    </li>
            </ul>
        </div>
    </div>
    <div class="section-3" id="features">
        <div class="container">
            <a class="back-to-selection" href="#main_selection" data-scroll-location="true">Back to Top&nbsp;<i class="fas fa-arrow-up"></i></a>
            <h1 class="text-center">Features</h1>
            <p class="text-justify">
                Why just fax your presentation to your next meeting? You expend significant thought and time into building a convincing presentation, only to have your audience flip directly to the last page. 
                <b>THT Web Worldwide Web Show</b> gives you control of the presentation and flow of information. 
            <p>
            <p class="text-justify">
                You can present across the street or across the globe. Death to distance and new life to your ability to make your presentations, meetings and successes even more memorable. 
            </p>
            <p class="text-justify">
                When should you use THT Web Worldwide Web Show?
            </p>
            <div class="m-5">
                <p class="text-justify">
                    <b>THT Web Worldwide Web Show</b> is loaded with features to help you reduce travel time and expense. It can be combined with any of our conference call solutions to effectively replace face-to-face meetings:
                </p>
                <ul>
                        <li>Marketing Events 
                        </li><li>Sales Presentations 
                        </li><li>Product Launches 
                        </li><li>Training Sessions 
                        </li><li>Financial Earnings Reviews 
                        </li><li>Policy Announcements 
                        </li><li>Follow up after 'Live' Meetings 
                        </li>
                </ul>
            </div>
            <p class="text-justify">
                <b>How does it work?</b>
            </p>
            <div class="m-5">
                <p class="text-justify">
                    It's easy! Your audience views your presentation on their computers via the web while you control the slides from your PC. For the audio portion of your presentation, dial into a THTWeb conference call. We'll provide you with expert support and step-by-step instructions.     
                </p>
            </div>
            <p class="text-justify">
                <b>Does it require special equipment?</b>
            </p>
            <div class="m-5">
                <p>
                    No, just basic business tools:
                </p>
                <ul>
                    <li>Two phone lines: one for your conference call and the other for an Internet connection. 
                    </li><li>A Java-enabled web browser such as Netscape Navigator 4.0 or higher or Microsoft Internet Explorer 4.0 or higher. 
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-4" id="streaming_media">
        <div class="container">
            <a class="back-to-selection" href="#main_selection" data-scroll-location="true">Back to Top&nbsp;<i class="fas fa-arrow-up"></i></a>
            <h1 class="text-center">Streaming Media</h1>
            <p class="text-justify">
                Looking for a way to relay your message to the masses? Streaming from THTWeb can help you do just that. It's ideal when you need to broadcast your message to the widest audience possible for investor relations calls, press conferences and other important announcements. Stream your conference live or have it archived for later playback. Stream your entire presentation, slides and all, or just the audio portion of your call - the choice is yours. 
            </p>
            
            <h2>Audio</h2>
            <p class="text-justify">
                Broadcast the audio portion of your conference and your audience will hear your voice over the Internet using a free streaming media player, such as Windows MediaTMPlayer or RealPlayer©.
            </p>
            <div class="m-5">
                <ul>
                    <li>
                        <h3>Flexible Archiving</h3>
                        <p class="text-justify">
                            Archived events help increase web site stickiness and provide a historical record of your communication. Archives are available for playback within 24 hours after the end of the call. You can create an archived stream from just about any type of media, including tapes, CDs and digital recordings. 
                        </p>
                    </li>
                    <li>
                        <h3>Editable Content</h3>
                        <p class="text-justify">
                            Manage the content of your archived conferences. THTWeb Streaming lets you download the archived audio file, edit it, then upload the new edited version. 
                        </p>
                    </li>
                    <li>
                        <h3>Scalable Events</h3>
                        <p class="text-justify">
                            Automatic event scaling to meet your streaming capacity needs. Invite thousands to attend your event and take as much time as you need to deliver your message - you will not get cut short and no one will get blocked out. 
                        </p>
                    </li>
                    <li>
                        <h3>Registration Page</h3>
                        <p class="text-justify">
                            View a list of the guests who have joined your call. Collect any type of information from your audience, such as company name, email address or whatever pertains to your event. 
                        </p>
                    </li>
                    <li>
                        <h3>Security</h3>
                        <p class="text-justify">
                            Your Registration Page can be protected by a password, ensuring that your conference will be attended by invited guests only.    
                        </p>
                    </li>
                </ul>
            </div>

            <h2>Audio Plus Slides</h2>
            <p class="text-justify">
                Streaming with slides is available using our service, THTWeb's multimedia event conferencing solution. Your audio and slides are automatically synchronized to create a seamless presentation. In addition to all of our audio features, adding slides gives you several other options for your stream:
            </p>
            <div class="m-5">
                <ul>
                    <li>
                        <h3>Customized Presentations</h3>
                        <p class="text-justify">
                            THTWeb's custom content services can assist you in creating a presentation that ranges from a basic PowerPoint to a full multimedia presentation, including video or Flash animation. The possibilities are endless.  
                        </p>
                    </li>
                    <li>
                        <h3>Invitations</h3>
                        <p class="text-justify">
                            Send Outlook-integrated email invitations and reminders to your audience to inform them of important show information and increase your event's attendance levels. 
                        </p>
                    </li>
                    <li>
                        <h3>Additional Features</h3>
                        <p class="text-justify">
                            Encourage audience participation with Chat, Polling and Post Conference Surveys. You determine which features your audience will be able to access.
                        </p>
                    </li>
                </ul>
            </div>

            <p class="text-justify">
                <b>Does it require special equipment? </b>
            </p>
            <div class="m-5">
                <p class="text-justify">
                    No special equipment is needed to initiate or attend a stream, just a PC with connection to the Internet and speakers. To notify participants, send the link via email or post it to your company web site. They click on the link and your message comes alive!
                </p>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection