@extends('layouts.layout')


@section('title')
Home
@endsection

@section('styles')
    <link href="https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer" rel="stylesheet">
    <link rel="stylesheet" href="{{mix('css/index_style.css')}}">
@endsection


@section('content')
    <div class="featurette-1">
        <div class="specialize container">
            <div class="row">
                <div class="col-md-7">
                    <h1>"When it's time to make the right call... go THTWeb"</h1>
                    <div class="list-header text-center">We specialize in </div>
                    <h2>    
                        <ul>
                            <li>Conference Calls</li>
                            <li>Audio Conferences</li>
                            <li>Web Conferencing</li> 
                            <li>Customized Multi-Person Communications</li>
                        </ul>
                    </h2>
                    <a href="#" class="learn-more-btn" data-toggle="modal" data-target="#signup_modal">
                        Find Out More
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="featurette-2">
        <div class="container">
            <h1><b>Features</b></h1>
            <div class="row feature-list">
                <div class="col-6 col-sm-6 col-md-4">
                    <div class="feature-item text-center">
                        <div class="icon-circle">
                            <div class="inner-circle">
                                <i class="fas fa-phone"></i>
                            </div>
                        </div>
                        <br/>
                        <h3>
                            Reservationless Conference Calls
                        </h3>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4">
                    <div class="feature-item text-center">
                        <div class="icon-circle">
                            <div class="inner-circle">
                                <i class="fas fa-user-friends"></i>
                            </div>
                        </div>
                        <br/>
                        <h3>
                            Operator Assisted
                        </h3>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4">
                    <div class="feature-item text-center">
                        <div class="icon-circle">
                            <div class="inner-circle">
                                <i class="fas fa-globe"></i>
                            </div>
                        </div>
                        <br/>
                        <h3>
                            Web Conferencing
                        </h3>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4">
                    <div class="feature-item text-center">
                        <div class="icon-circle">
                            <div class="inner-circle">
                                <i class="fab fa-microsoft"></i>
                            </div>
                        </div>
                        <br/>
                        <h3>
                            Microsoft Outlook Integration
                        </h3>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4">
                    <div class="feature-item text-center">
                        <div class="icon-circle">
                            <div class="inner-circle">
                                <i class="fas fa-hand-holding-usd"></i>
                            </div>
                        </div>
                        <br/>
                        <h3>
                            Web And/Or Audio From Around The World And Pay in US Dollars
                        </h3>
                    </div>
                </div>
                <div class="col-6 col-sm-6 col-md-4">
                    <div class="feature-item text-center">
                        <div class="icon-circle">
                            <div class="inner-circle">
                                <i class="fas fa-globe-americas"></i>
                            </div>
                        </div>
                        <br/>
                        <h3>
                            International Toll-free Origination From 50 Countries
                        </h3>
                    </div>
                </div>
            </div>
            <h1><b>What we offer</b></h1>
        </div>
    </div>
    <div class="featurette-3">
        <div class="arrow-scroll-down"></div>
        <div class="container">
          
            <div class="row">
                <div class="col-lg-6 content-center">
                    <div class="inner-content">
                        <h1><b>Audio Conferencing</b></h1>
                        <p class="text-justify">
                            Need to pull everyone together for a conference call? THTWeb is ready. Our complete range of telephone conferencing solutions can handle any of your conferencing requirements. 
                        </p>
                        <a href="{{url('/solutions/audio')}}" class="learn-more-btn">
                            Learn More&nbsp;<i class="fas fa-arrow-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <img class="img-fluid" src="{{asset('images/audio-beard-blur-1005762.jpg')}}" alt="audio conferencing">
                </div>
            </div>
        </div>
    </div>
    <div class="featurette-4">
        <div class="arrow-scroll-down"></div>
        <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <img class="img-fluid" src="{{asset('images/adult-asking-blur-630839.jpg')}}" alt="web conferencing">
                    </div>
                    <div class="col-lg-6 content-center">
                        <div class="inner-content">
                            <h1><b>Web Conferencing</b></h1>
                            <p class="text-justify">
                                The ability to use visual communications improves your efficiency and effectiveness. Allow THTWeb to help you improve:
                            </p>
                            <div class="row">
                                <div class="col-6">
                                    <ul>
                                        <li>
                                            Training Seminars
                                        </li>
                                        <li>
                                            Sales Presentations
                                        </li>
                                        <li>
                                            Product Launches
                                        </li>
                                        <li>
                                            Announcements
                                        </li>  
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <ul>
                                        <li>
                                                Financial Reports
                                        </li>
                                        <li>
                                                Board Presentations for out-of-town Board Members
                                        </li>   
                                        <li>
                                                Virtual Tours
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        
                            <a href="{{url('/solutions/web')}}" class="learn-more-btn">
                                Learn More&nbsp;<i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="featurette-5">
        <div class="arrow-scroll-down"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 content-center">
                        <div class="inner-content">
                            <h1><b>International Conferencing</b></h1>
                            <p class="text-justify">
                                Expand your global reach with THTWeb's International Conferencing Solutions. Select from our options to connect your international business partners to a U.S. conference call.
                            </p>
                            <a href="{{url('/solutions/international')}}" class="learn-more-btn">
                                Learn More&nbsp;<i class="fas fa-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img class="img-fluid" src="{{asset('images/atlas-ball-shaped-business-269633.jpg')}}" alt="int'l audio conferencing">
                    </div>
                </div>
            </div>
    </div>
    <div class="featurette-6">
        <div class="arrow-scroll-down"></div>
        <div class="container">
            <div class="question-mark">
                <i class="fas fa-question"></i>
            </div>
            <p>
                <b>Need to find out more about THTWeb?</b> 
            </p>
            <a href="#" data-toggle="modal" data-target="#signup_modal">
                Sign Up Now!
            </a>
        </div>
    </div>
@endsection

@section('scripts')

@endsection