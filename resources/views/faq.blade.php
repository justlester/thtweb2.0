@extends('layouts.layout')


@section('title')
FAQ
@endsection

@section('styles')
<link rel="stylesheet" href="{{mix('css/faq_style.css')}}">
@endsection

@section('content')
<div class="section-1">
    <div class="container">
        <h1>Frequently Asked Questions</h1>
        <div class="card">
            <div class="card-body">
                <div class="faq-list">
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                It's hard to keep track of who's on our call because people are often "popping in" and "dropping off." It gets tiring to ask "Who just joined?" or "Who just left?" or "OK, who's on the call now?". Can you help? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                Relax. With THTWeb's automated roll call and announce feature, all callers will be identified, and you won't need to stop the call for a roll call.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                How does THTWeb support my account? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                We pride ourselves on our customer service and support. Our account managers are available during normal business hours and our customer support center and reservation operators are on call 7 days per week, 24 hours per day. When you sign up for our service you will be given a toll free number for Customer service, Technical support and reservations.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                How long will my calls be available? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                THTWeb offers free archiving of recorded calls for 30 days. If you'd like, we can even record them to a CD ROM disk.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                How do I make records of my calls? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                THTWeb provides transcription services using bonded and secured transcribers to provide security and reliability in documenting your call content.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                How do I keep track of the dial-in numbers? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                THTWeb makes it simple because you will have the same toll-free number for all of your calls. You can even share your toll-free access number with up to 10 call leaders within your company. Each call leader will be assigned a different 10-digit account number to keep billing organized by user or department.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                Will I have to memorize a 10-digit code?  
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                THTWeb wants to make it easy, so we match your account code to your 10-digit phone number for convenience and simplicity.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                Will others be able to use my conference number without my permission?  
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                No. In order to start the call, you must have the leader PIN. If you'd like to delegate this ability to others, you can. THTWeb provides you with the flexibility to make that decision. Each call leader will have access to THTWeb's on-line account management center where leaders will be able to manage the options on your account.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                What if I use THTWeb's automated platform and I need assistance? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                Not to worry! Unlike the many so-called bargain automated products out there, THTWeb provides operator availability on your automated Reservationless calls. We want your experience to be productive and effective, so we will always be there if you need us.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                Who will be responsible for my account?  
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                Your salesperson will always be responsible to ensure that you receive quality support from our customer service center. We place a high value on the relationship you create with your salesperson and want you to have a direct contact to them in addition to the customer service department.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                Do I have the ability to view my account on-line? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                THTWeb provides customers with the ability to view and configure their accounts on-line using a secure, password-protected interface.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="question">
                            <div class="question-letter">
                                Q:
                            </div>
                            <div class="question-content">
                                I am an attorney who needs to keep track of my clients so I can be sure to bill accurately for my services. How can you help? 
                            </div>
                        </div>
                        <div class="answer">
                            <div class="answer-letter">
                                A:
                            </div>
                            <div class="answer-content">
                                THTWeb offers customers project accounting codes with up to 19 digits to allow your firm to use its existing client codes. At the end of each month, we will provide you with a breakdown, by code, of your conferencing usage.
                            </div>
                        </div>
                    </div>
                    <div class="faq-item">
                            <div class="question">
                                <div class="question-letter">
                                    Q:
                                </div>
                                <div class="question-content">
                                    We have a large number of invitees on our calls and someone always misses the call. Any suggestions? 
                                </div>
                            </div>
                            <div class="answer">
                                <div class="answer-letter">
                                    A:
                                </div>
                                <div class="answer-content">
                                    THTWeb offers its customers on-demand recording capability. Unlike WorldCom and AT&T, THTWeb makes this service available on our automated Reservationless platform. With the touch of two keys, you can begin recording your calls. This service is also great for training, marketing roll-outs and legal depositions. Your calls will be archived and will be available for future playback. Contacts will be able to dial-in or listen to the call over the Internet.
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection