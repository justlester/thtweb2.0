@extends('layouts.layout')


@section('title')
About Us
@endsection

@section('styles')
    <link href="https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400i" rel="stylesheet">
    <link rel="stylesheet" href="{{mix('css/about_style.css')}}">
@endsection


@section('content')
<div class="section-1">
    <div class="container">
        <div class="our-mission">
            <h1>Our Mission</h1>
            <p>
                To provide our customers with technology that enhances a user's ability to collaboratively communicate with greater reach, scale and effectiveness. We offer products that span the spectrum from easily adoptable technology thru leading edge products that converge voice, video and web. 
            </p>
        </div>
    </div>
</div>
<div class="section-2">
    <div class="container">
        <h1>What is THTWeb?</h1>
        <p>
            THTWeb develops, deploys and discounts the finest conferencing product set available on the web. Attention to detail and painstaking efforts to ensure smooth and trouble-free conferences have underscored our commitment to quality service since we began. Our rapid growth in this space is the result of our customers' high level of trust and confidence in the ability of THTWeb to deliver on our promise of quality, automated, operator-assisted web conferencing and audio and video streaming solutions at a discounted price. 
        </p>
        <p>
            Our platform of over 24,000 available ports assures you the power of conferencing connectivity when you need it. Our global network allows you to stream your voice or data across the Internet and reach your audience at a greatly reduced cost-per-conference over traditional international voice dial-in rates. We can stream in real-time or archive your calls and/or content for on-demand distribution. Our service is great for training and for calls that are adversely impacted by global time zone differences. 
        </p>
        <p>
            From simple conference calls to complex, large-scale global streaming of rich content, our customers say: 
        </p>
        <h2>
            "When it's time to make the right call... go THTWeb"
        </h2>
    </div>
</div>
<div class="section-4">
    <div class="container">
        <hr/>
        <h1>Management</h1>
        <br/>
        <div class="row">
            <div class="col-md-4 mgt-photo-container">
                <img class="mgt-photo" src="https://s3-us-west-2.amazonaws.com/archintel-bucket/images/Jim-Garrettson.jpg" alt="Jim Garretson">
            </div>
            <div class="col-md-8">
                <h2>Jim Garrettson</h2>
                <h5>President and CEO</h5>
                <br/>
                <p>
                    He founded THTWeb following a successful 20-year career in telecommunications. Although the Telecom market has been in a downturn recently, Jim is a firm believer in the future of collaborative communications. Specifically, he has founded THTWeb with the intent of spreading the enabling power of the collaborative IP-based tools that have been recently brought to market. 
                </p>
                <p>
                    In addition to bringing this organization to a quick success, Jim has also founded and created "The Potomac Officers Club" (POC). This Washington, DC-based club is unique in its formation as an organization designed specifically for senior executives. The POC has been growing steadily under his leadership with its mission of creating a board of trade to foster business growth in the Greater Washington area. Beyond his current duties at The Potomac Officers Club and THTWeb, Garrettson produces a widely-read newsletter for area executives. 
                </p>
                <p>
                    Prior to founding THTWeb, Garrettson held the position of EVP of Sales and Marketing at Net2000, a competitive local exchange carrier. Garrettson has also gained experience in the start-up and Internet content distribution space as SVP of North American Sales with Cidera. He helped Cidera expand their satellite-based, edge network throughout North America. Before Cidera, Garrettson held the position of President, Teleglobe Business Solutions and SVP of Sales at Qwest Communications.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="section-5">
    <div class="container">
        <h1>Headquarters & Contact</h1>
        <div class="contact-blob">
            <div class="contact-icon">
                <div class="inner-contact-icon">
                    <i class="fas fa-map-marker-alt"></i>
                </div>
            </div>
            <div class="contact-content">
                <div class="inner-contact-content">
                        THTWeb<br/>
                        8230 Old Courthouse Road, Suite 460<br/>
                        Vienna, VA 22182<br/>
                </div>
            </div>
        </div>
        <div class="contact-blob">
            <div class="contact-icon">
                <div class="inner-contact-icon">
                   <i class="fas fa-fax"></i>
                </div>
            </div>
            <div class="contact-content">
                <div class="inner-contact-content">
                    <table>
                        <tr>
                            <td>Toll Free:</td>
                            <td>(866) 633-8453</td>
                        </tr>
                        <tr>
                            <td>Main:</td> 
                            <td>(703) 752-1200</td>
                        </tr>
                        <tr>
                            <td>Fax:</td>
                            <td>(703) 752-7454</td>
                        </tr>
                    </table>   
                </div>
            </div>
        </div>
        <div class="contact-blob">
            <div class="contact-icon">
                <div class="inner-contact-icon">
                    <i class="fas fa-envelope"></i>
                </div>
            </div>
            <div class="contact-content">
                <div class="inner-contact-content">
                    Email: <a href="mailto:info@thtweb.com">info@thtweb.com</a>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')

@endsection