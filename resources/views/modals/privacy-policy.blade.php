<div class="modal fade" id="privacy_policy_modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="btn custom-close" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times"></i>
            </button>
            <h1 class="modal-title">Privacy Policy</h1>
            <p class="text-justify">
                This is how we will handle information we learn about you from your visit to our web site. The information we receive depends upon what you do when visiting our site. If you visit our site to read or download information, such as consumer brochures or press releases: 
            </p>
            <p class="text-justify">
                We collect and store only the following information about you: the name of the domain from which you access the Internet (for example, aol.com, if you are connecting from an America Online account, or princeton.edu if you are connecting from Princeton University's domain); the date and time you access our site; and the Internet address of the web site from which you linked directly to our site. We do not store any personal identifiers. This information is not stored in any particular fashion, but is part of a systems log file. 
            </p>
            <p class="text-justify">
                For site management, information is collected for statistical purposes. This computer system uses software programs to create summary statistics, which are used for such purposes as assessing what information is of most and least interest, determining technical design specifications, and identifying system performance or problem areas. We use the information we collect to measure the number of visitors to the different sections of our site and to help us make our site more useful to visitors. This information is only used internally, does not contain personal identifiers, and will only be used for internal management purposes. 
            </p>
            <p class="text-justify">
                If you identify yourself by sending an E-mail or filling out a questionnaire or guestbook:
            </p>
            <p class="text-justify m-5">
                You also may decide to send us personally-identifying information, for example, in an electronic mail message containing a complaint. We use personally-identifying information from consumers in various ways to further our customers' protection and to provide better service. This may be to correct broken links that you have identified or provide additional information that you have requested.
            </p>
            <p class="text-justify">
                You can contact us by postal mail, telephone, or electronic mail. You may fill out a questionnaire or guestbook. Before you do, there are a few things you should know.
            </p>
            <p class="text-justify">
                The material you submit may be seen by various people. We may enter the information you send into our electronic 
                <b>database created for certain programs, such as a regulations commenting system that allows the public to comment on proposed regulations</b>
                . This information may be used to determine how to better serve our public or obtain feedback on "what you want to see", "how we are doing", or 
                <b>obtain comments on draft policy</b>
                . This information will not be shared with any other program or organization.
            </p>
            <p class="text-justify">
                <b>Any information that we collect via a questionnaire or a guestbook may be subject to disclosure, but will be handled in accordance with the requirements of the <a href="http://www.usdoj.gov/04foia/04_7_1.html" target="_blank">Privacy Act</a> and the <a href="http://www.doi.gov/foia/foiaregs.html" target="_blank">Freedom of Information Act</a> to ensure the greatest protection of personal privacy in the face of any required disclosure. </b>
            </p>
            <p class="text-justify">
                    Also, e-mail is not necessarily secure against interception. If your communication is very sensitive or includes personal information like your bank account, charge card, or social security number, you might want to send it by postal mail instead. 
            </p>
            <p class="text-justify">
                We want to be very clear: we will not obtain personally-identifying information about you when you visit our site, unless you choose to provide such information to us. Except as might be required by law, we do not share any information we receive with outside parties.
            </p>
        </div>
    </div>
  </div>
</div>