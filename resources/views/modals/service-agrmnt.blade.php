<div class="modal fade" id="service_agrmnt_modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="btn custom-close" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times"></i>
            </button>
            <h1 class="modal-title">Service Agreement</h1>
            <p class="text-justify">
                THIS SERVICES AGREEMENT, together with the signature page, addenda, Price Quote Sheet, and Service Exhibit(s) attached hereto (collectively, the “Agreement”) is entered into by and between THTWeb. (“THT ” or “THTWeB” or “THT Web Worldwide”), located at 2301 Gallows Rd Vienna, Va. 22027, and your corporation, (the “Customer”). The following Terms and Conditions shall be applicable to Customer’s purchase and use of network services (“THTWeB Services”) offered by THTWeb and ordered by Customer under any sales order form and/or Price Quote Sheet (“Customer Order”). The Customer Order will detail the rates and charges regarding Customer’s purchase of THTWeb’s Services. 
            </p>
            <p class="text-justify">
                <b>1.</b> Entire Agreement. This Agreement, together with all addenda and Customer Orders, constitutes the entire agreement between the Parties with respect to the subject matter hereof, and supersedes any and all prior offers, contracts, agreements, representations and understandings made to or with Customer by THTWeb or any predecessors-in-interest, whether oral or written. All amendments to this Agreement shall be in writing and signed by the Parties. 
            </p>
            <p class="text-justify">
                <b>2.</b> Revenue Commitments. All revenue commitments, if any, agreed upon by THTWeb and Customer applicable to the Services shall be set forth in the Customer Order. 
            </p>
            <p class="text-justify">
                <b>3.</b> Service Requests. All Service order requests or cancellations require Customer’s completion and THTWeB’s acceptance of THTWeB’s Service order forms issued by THTWeb from time to time. Other than alternative rates mutually agreed upon by the Parties in any Service order, the terms and conditions of this Agreement shall supercede any inconsistent terms and conditions contained in a Customer Order
            </p>
            <p class="text-justify">
                <b>4.</b> Taxes. In addition to all other charges set forth in the applicable Customer Order, Customer shall pay any and all applicable foreign, federal, state and local taxes, including without limitation, all use, sales, value-added, surcharges, excise, franchise, property, commercial, gross receipts, license, privilege or other similar taxes, levies, surcharges, duties, fees, or other tax-related surcharges, whether charged to or against THTWeb or Customer, with respect to the Services or underlying facilities provided by THTWeb . lesser rate of one and a half percent (1.50%) per month or the maximum lawful rate allowable under applicable state law. If Customer fails to pay for Services in accordance with the terms set forth herein, in addition to its termination rights under Section 8, THTWeb may without notice: (i) refuse to accept additional orders for Services; (ii) suspend provisioning any or all of the Services until Customer has paid all past due amounts (including interest); and (iii) offset such unpaid balances from any amounts that THTWeb owes to Customer under any other agreements between the Parties. During any period of suspension, no service interruption shall be deemed to occur.
            </p>
            <p class="text-justify">
                <b>5.</b> Billing. For the Services provided pursuant to this Agreement, Customer shall pay THTWeb per the pricing and provisions set forth in this Agreement. THTWeb shall provide a monthly invoice for the Services and the invoiced amounts shall be due and payable in U.S. Dollars in immediately available funds within thirty (30) days from the date of invoice. All amounts which are not paid by the due date stated on Customer’s bill shall be subject to a late payment charge. The unpaid balance of any past due bills shall bear interest at a rate of 1.5% per month (prorated on a daily basis), or the highest rate allowable by law, whichever is less. Although it is the intent of THTWeb to provide timely and accurate bills to Customer, failure of THTWeb to provide bills in a timely or accurate manner shall not constitute breach or default of this Agreement. If Customer in good faith disputes any portion of the charges contained in a bill, Customer must pay the invoice in full and submit a documented claim for the disputed amount. All claims must be submitted to the invoicing Party within sixty (60) days of the date of invoice for those Services. If Customer does not submit a claim within such period and in the manner stated above, Customer waives all rights to dispute such charges
            </p>
            <p class="text-justify">
                <b>6.</b> Rates. All rates are subject to change immediately, with no prior notice to Customer, in the event there are mandated surcharges imposed by a federal, state or governmental agency. Further, notwithstanding any statements to the contrary contained in the Tariff, in the event of Regulatory Activity, THTWeb reserves the right, at any time upon written notice, to: (i) pass through to Customer all, or a portion of, any charges or surcharges directly or indirectly related to such Regulatory Activity; or (ii) modify the rates, including any rate guarantees, and/or other terms and conditions contained in the Agreement and/or the Tariff to reflect the impact of such Regulatory Activity. THTWeb may adjust its rates or charges, or impose additional rates and charges, in order to recover amounts it may be required by governmental or quasi-governmental authorities to collect from or pay to others to support statutory or regulatory programs during the course of the Agreement. Rate increases are subject to change with five (5) days notice. Rate decreases are effective as stated in written notice from THTWeb. 
            </p>
            <p class="text-justify">
                <b>7.</b> Term. This Agreement shall be effective upon the Effective Date and continue until the expiration or termination of all Service Exhibits. 
            </p>
            <p class="text-justify">
                <b>8.</b> Termination. 
            </p>
            <div class="m-5">
                <p class="text-justify">
                    <b>8.1</b> Termination for Insolvency or Change of Control. THTWeb may immediately terminate this Agreement without notice if Customer: (i) becomes or is declared insolvent or bankrupt; (ii) is the subject of any proceedings related to its liquidation, insolvency or for the appointment of a receiver or similar officer for it; (iii) makes an assignment for the benefit of all or substantially all of its creditors; (iv) enters into an agreement for the composition, extension, or readjustment of all or substantially all of its obligations; or (v) Customer has a Change of Control. 
                </p>
                <p class="text-justify">
                    <b>8.2</b> Termination for Cause. Except as otherwise provided for herein, either Party may terminate a Customer Order for Cause, or if Cause exists to terminate all or substantially all of the Services, then such Party may terminate the Agreement in its entirety. If Cause exists as a result of Customer’s failure to satisfy its payment obligations under the Customer Order, then THTWeb may terminate the Agreement in its entirety for Cause with no further liability or obligation to Customer. In the event that dedicated Internet access is provided to Customer, termination will occur immediately upon a violation of the AUP. 
                </p>
            </div>
            <p class="text-justify">
                <b>9.</b> Limitation of Liability and Warranties. 
            </p>
            <div class="m-5">
                <p class="text-justify">
                    <b>9.1</b> Disclaimer of Warranties. EXCEPT AS SPECIFICALLY SET FORTH IN THE CUSTOMER ORDER, THTWeb MAKES NO WARRANTIES, EXPRESS OR IMPLIED, AS TO ANY SERVICE PROVISIONED HEREUNDER. THTWeb SPECIFICALLY DISCLAIMS ANY AND ALL IMPLIED WARRANTIES; INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR TITLE OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS.  
                </p>
                <p class="text-justify">
                    <b>9.2</b> Disclaimer of Certain Damages. WITHOUT LIMITING ANY EXPRESS LIABILITY PROVISIONS PROVIDED FOR IN THIS AGREEMENT, NEITHER PARTY SHALL BE LIABLE TO THE OTHER FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, EXEMPLARY, SPECIAL, INCIDENTAL OR PUNITIVE DAMAGES, INCLUDING WITHOUT LIMITATION, LOSS OF USE OR LOST USINESS, REVENUE, PROFITS, OR GOODWILL, ARISING IN CONNECTION WITH THIS AGREEMENT AND/OR THTWeb ’S PROVISIONING OF THE SERVICES (INCLUDING ANY SERVICE IMPLEMENTATION DELAYS/FAILURES), AND FOR THE APPLICABLE SPACE, UNDER ANY THEORY OF TORT, CONTRACT, WARRANTY, STRICT LIABILITY OR NEGLIGENCE, EVEN IF THE PARTY HAS BEEN ADVISED, KNEW OR SHOULD HAVE KNOW OF THE POSSIBILITY OF SUCH DAMAGES.
                </p>
                <p class="text-justify">
                    <b>9.3</b> Limitation of THTWeB’s Liability. WITHOUT LIMITING THE PROVISIONS OF SECTION 9.2 ABOVE, THE TOTAL LIABILITY OF THTWeb TO CUSTOMER IN CONNECTION WITH THIS AGREEMENT SHALL BE LIMITED TO THE AGGREGATE AMOUNT OF OUTAGE CREDITS DUE UNDER THE CUSTOMER ORDER FOR THE AFFECTED SERVICE, OR ALTERNATIVELY IF NO OUTAGE CREDITS ARE PROVIDED FOR IN SUCH CUSTOMER ORDER THEN PURSUANT TO THTWeB’S THEN CURRENT OUTAGE CREDIT POLICIES. THE FOREGOING LIMITATION APPLIES TO ALL CAUSES OF ACTIONS AND CLAIMS, INCLUDING WITHOUT LIMITATION, BREACH OF CONTRACT, BREACH OF WARRANTY, NEGLIGENCE, STRICT LIABILITY, MISREPRESENTATION AND OTHER TORTS. FURTHER, THTWeB’S LIABILITY, WITH RESPECT TO INDIVIDUAL SERVICES, MAY ALSO BE LIMITED PURSUANT TO THE TERMS AND CONDITIONS OF THE CUSTOMER ORDER. CUSTOMER ACKNOWLEDGES AND ACCEPTS THE REASONABLENESS OF THE FOREGOING DISCLAIMER AND LIMITATIONS OF LIABILITY. NO CAUSE OF ACTION UNDER ANY THEORY WHICH ACCRUED MORE THAN ONE (1) YEAR PRIOR TO THE INSTITUTION OF A LEGAL PROCEEDING ALLEGING SUCH CAUSE OF ACTION MAY BE ASSERTED BY EITHER PARTY AGAINST THE OTHER. 
                </p>
                <p class="text-justify">
                    For purposes of this Section 9, all references to THTWeb and Customer include their respective Affiliates, End Users, agents, officers, directors, shareholders and employees. 
                </p>
                <p class="text-justify">
                    <b>9.4</b> THTWeb shall not be liable for any fraudulent calls or usage of the Services or Customer’s services and which may be included on Customer’s Invoices. THTWeb has no obligation to investigate the authenticity of any call or Service usage charged to Customer’s account. Customer acknowledges that THTWeb regular or everyday network maintenance may result in Service interruptions. 
                </p>
                <p class="text-justify">
                    <b>9.5</b> If network maintenance should result in the interruption of Service, to the extent possible it shall be accomplished after notification to Customer and will be completed within a reasonable time. 
                </p>
            </div>
            <p class="text-justify">
                <b>10.</b> Indemnity. Customer agrees to indemnify, defend and hold harmless THTWeb and its Affiliates, from and against any and all claims, actions, damages, liabilities, costs, judgments, expenses, costs of litigation, investigation or proceeding (including reasonable attorney fees and an allocated share of in-house counsel fees) (the “Losses”) arising out of or in connection with Customer’s use, resale or sharing of the Services, including but not limited to any instance of unauthorized changes of service to actual or prospective End Users or customers. 
            </p>
            <p class="text-justify">
                <b>11.</b> Relationship. Neither Party shall have the authority to bind the other by contract or otherwise or make any representations or guarantees on behalf of the other. Both Parties acknowledge and agree that the relationship arising from this Agreement is one of independent contractor, and does not constitute an agency, joint venture, partnership, employee relationship or franchise.             
            </p>
            <p class="text-justify">
                <b>12.</b> Assignment or Sale. This Agreement shall be binding on Customer and its respective Affiliates, successors, and assigns. Customer shall not assign, sell or transfer this Agreement or the right to receive the Services provided hereunder, whether by operation of law or otherwise, without the prior written consent of THTWeb, which consent shall not be unreasonably withheld or delayed. Any attempted assignment in violation hereof shall be null and void. 
            </p>
            <p class="text-justify">
                <b>13.</b> Survival. The expiration or termination of this Agreement shall not relieve either Party of those obligations that by their nature are intended to survive.
            </p>
            <p class="text-justify">
                <b>14.</b> Severability. If any provision of this Agreement is held to be invalid or unenforceable, the remainder of the Agreement will remain in full force and effect. If any such provision may be made enforceable by a limitation of its scope or time period, such provision will be deemed to be amended to the minimum extent necessary to render it enforceable.
            </p>
            <p class="text-justify">
                <b>15.</b> Notices. Except as otherwise provided herein, any notice required pursuant to this Agreement shall be in writing, transmitted to the Parties’ addresses specified in the signature page or such other addresses as may be specified by written notice, and will be considered given either: (i) when delivered by facsimile or e-mail, so long as duplicate notification is sent via regular U.S. Mail or overnight delivery within a reasonable time thereafter; (ii) when delivered in person to the recipient named on the signature page; (iii) when deposited in either registered or certified U.S. Mail, return receipt requested, postage prepaid; or (iv) when delivered to an overnight courier service. Any notice regarding rate change will be effective if made via facsimile, the facsimile confirmation sheet will serve as receipt of rate change notice. 
            </p>
            <p class="text-justify">
                <b>16.</b> Force Majeure/System Maintenance. Neither Party shall be liable to the other for any delay or failure in performance of any part of this Agreement to the extent that such delay or failure is caused by a Force Majeure Event. The Party claiming relief under this Section shall notify the other in writing of the existence of the Force Majeure Event relied on and shall be excused on a day-by-day basis to the extent of such prevention, restriction or interference until the cessation or termination of said Force Majeure Event. In addition, THTWeb will use reasonable efforts during the Term of this Agreement to minimize any Service interruptions that might occur as a result of planned system maintenance required to provision the Services.
            </p>
            <p class="text-justify">
                <b>17.</b> Governing Law. This Agreement is governed by and shall be construed in accordance with the laws of the State of New York without regard to its choice of law principles, except and to the extent that the Communications Act of 1934, as amended and interpreted by the FCC, applies to this Agreement. THTWeb reserves the right to suspend and/or terminate any Service without liability where: (i) Regulatory Activity prohibits, restricts or otherwise prevents THTWeb from furnishing such Service; or (ii) any material rate, charge or term of such Service is substantially changed by a legitimate regulatory body, governmental authority, or by order of the highest court of competent jurisdiction to which the matter is appealed.  
            </p>
        </div>
    </div>
  </div>
</div>