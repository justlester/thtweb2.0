<div class="modal fade" id="signup_modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
 
        <div class="modal-body">
            <button type="button" class="btn sign-up-close" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times"></i>
            </button>
            <h1 class="sign-up-title">
                Sign Up
            </h1>
            <p class="text-justify sign-up-instruct">
                Please fill out the information below and one of our customer service agents will get in touch with you to set you up with a FREE two-week trial of our web conferencing product, THT WebShow. 
            </p>
            <form>
                <div class="required">This field is required</div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Name">
                </div>
                <div class="required">This field is required</div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-building"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Company">
                </div>
                <div class="required">This field is required</div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-at"></i></span>
                    </div>
                    <input type="email" class="form-control" placeholder="Email">
                </div>
                <div class="required">This field is required</div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                    </div>
                    <input type="number" class="form-control" placeholder="Phone">
                </div>
                <div class="required">This field is required</div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-flag"></i></span>
                    </div>
                    <select class="form-control">
                        <option value="" disabled selected>Please select a Country</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                </div>
                <div class="required">This field is required</div>
                <textarea rows="10" placeholder="Please describe your teleconferencing needs..."></textarea>
                <button type="button" type="submit" class="btn sign-up-submit">Submit</button>
            </form>
        </div>
    </div>
  </div>
</div>