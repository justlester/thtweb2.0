@extends('layouts.layout')


@section('title')
International Conferencing
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/con_int_style.css')}}">
@endsection

@section('content')
    <div class="section-1">
        <div class="container">
           
            <div class="card international-container">
                <div class="card-body">
                    <h1><b>International Conferencing</b></h1>
                    <p class="text-center">Need International Conferencing? THT Web Worldwide has your Voice, Audio, and Web Solutions</p>
                    <br/>
                    <br/>
                    <ul class="calling-plans-list">
                        <li class="row">
                            <div class="col-md-4 icon-container">
                                <div class="icon">
                                    <i class="fas fa-file-invoice-dollar"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2>
                                    <b>International Toll-Free Service (ITFS)</b> 
                                </h2>
                                <p class="text-justify">
                                    With International Toll-Free Service, you can provide convenient, toll-free access to your calls for overseas clients and employees. The ITFS phone number uses the originating country's national numbering format, which means the toll-free number will vary by country. Using the toll-free number assigned during reservation, your participants simply dial in at the appropriate time to join the conference with the assistance of a U.S. THTWeb Operator.
                                </p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-4 icon-container">
                                <div class="icon">
                                    <i class="far fa-clock"></i>
                                </div>
                            </div>  
                            <div class="col-md-8">
                                <h2 >
                                    <b>Deal with time zone differences</b> 
                                </h2>
                                <p class="text-justify">
                                    THT Web Worldwide offers you the ability to record calls, store them electronically and then allow your international users the option to access the recording when convenient in their time zone. You will get a detailed report on call playback. 
                                </p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-md-4 icon-container">
                                <div class="icon">
                                    <i class="fas fa-arrow-left"></i><i class="fas fa-phone"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h2>
                                    <b>International Dial-Out</b> 
                                </h2>
                                <p class="text-justify">
                                        Give your international participants the VIP treatment by bringing your U.S. conference to them. 
                                        You provide the participant's telephone numbers to a THTWeb Operator, who joins them to the call 
                                        at the designated time. Your international clients will be relieved of dialing pressures and impressed
                                        by the professional quality of your conference. International Dial-Out is available with either 
                                        <a href="{{url('/solutions/audio#operator_assisted')}}">Operator Assisted</a> or 
                                        <a href="{{url('/solutions/audio#reservationless_plus')}}">Reservationless-Plus</a> audio services. 
                                </p>
                            </div>
                        </li>
                    </ul>                   
                </div>
                <br/>
                <hr/>
                <br/>
                <div class="card-body">
                     <p class="text-justify">
                        As your business grows and evolves, THT Web Worldwide can assist with high quality conference calls that enable you to:
                    </p>
                    <ul>
                        <li> 
                            <b>Deliver new services</b> to customers and partners domestically and globally.
                        </li>
                        <li> 
                            <b>Exchange ideas</b> on proposals, presentations, and other business development activities regardless of time zones.
                        </li>
                        <li> 
                            <b>Train</b> employees, vendors, or clients down the block or across an ocean.
                        </li>
                        <li> 
                            <b>Conduct brainstorming</b>, negotiation, or planning sessions with people on all continents.
                        </li>
                        <li> 
                            <b>Gather feedback</b> from key participants no matter where they are.
                        </li>
                    </ul>
                        
                    <p class="text-center">
                        <b>If you are looking for international conferencing, THT Web Worldwide has got the world coverage you need!</b>
                    </p>
                    {{-- <p class="text-center">
                        Expand your global reach with THTWeb's International Conferencing Solutions. Check out our great rates to these international countries: 
                    </p>
            
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-argentina.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">Argentina</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-australia.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">Australia</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-austria.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">Austria</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-brazil.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">Brazil</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-chile.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">Chile</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-china.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">China</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-colombia.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">Colombia</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-denmark.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">denmark</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-finland.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">finland</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-france.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">france</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-germany.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">germany</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-hongkong.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">hong kong</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-india.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">india</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-ireland.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">ireland</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-israel.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">israel</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-italy.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">italy</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-japan.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">japan</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-mexico.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">mexico</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-thenetherlands.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">the netherlands</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-newzealand.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">new zealand</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-panama.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">panama</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-poland.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">poland</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-russia.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">russia</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-singapore.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">singapore</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-southafrica.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">south africa</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-southkorea.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">south korea</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-spain.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">spain</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-sweden.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">sweden</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-switzerland.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">switzerland</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-taiwan.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">taiwan</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-thailand.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">thailand</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-theunitedkingdom.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">the united kingdom</h5>
                                    </div>
                                </a>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 country-item-container">
                                <a class="card country-item" href="#">
                                    <img class="card-img-top" src="{{asset('images/flag-venezuela.png')}}" >
                                    <div class="card-body">
                                        <h5 class="card-title">venezuela</h5>
                                    </div>
                                </a>
                            </div>
                        </div> --}}
                  
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection