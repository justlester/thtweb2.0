@extends('layouts.layout')


@section('title')
Audio Conferencing
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/con_audio_style.css')}}">
@endsection

@section('content')
    <div class="section-1" id="main_selection">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h1><b>Audio Conferencing</b></h1>
                    <p class="text-center">
                        Browse our audio products & services below to determine which will best match your business needs. 
                    </p>
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <a class="scroll-links" data-scroll-location="true" href="#reservationless">
                                <div class="inner-scroll-links">
                                    Reservationless
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <a class="scroll-links" data-scroll-location="true" href="#operator_assisted">
                                <div class="inner-scroll-links">
                                    Operator Assisted
                                </div>
                            </a>
                        </div>
                        {{-- <div class="col-xs-6 col-sm-6 col-md-6">
                            <a class="scroll-links" data-scroll-location="true" href="#manage_event">
                                <div class="inner-scroll-links">
                                    Managed Event / IR
                                </div>
                            </a>
                        </div> --}}
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <a class="scroll-links" data-scroll-location="true" href="{{url('/solutions/international')}}">
                                <div class="inner-scroll-links">
                                    International Audio Conferencing
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-2" id="reservationless">
        <div class="container">
            <a class="back-to-selection" href="#main_selection" data-scroll-location="true">Back to Top&nbsp;<i class="fas fa-arrow-up"></i></a>
            <h1 class="text-center">Reservationless</h1>
            <p class="text-justify">
                You need to have the flexibility to conduct your calls at a moment's notice. THTWeb offers an automated conferencing platform with over 24,000 available port capacity that is always available to handle your call. It's dependable, convenient - and here's how we make it simple: 
            </p>
            <ul>
                <li>Your dial-in number will always be the same.</li>
                <li>Share Power Point slides while on your call.</li>
                <li>You have on-line access that enables you to manage and customize your conference preferences.</li>
                <li>Live operators are available on all calls by pressing "*0".</li>
                <li>You have the ability to record your call and stream it over the Internet.</li>
                <li>Up to 100 people can dial in at the same time using our reservationless service.</li>
            </ul>
            <h2>
                Additional Features
            </h2>
            <p class="text-justify">
                THTWeb's Reservationless service offers a variety of features to customize your conference, in addition to providing quick access to your call. Standard options include:  
            </p>
            <ul>
                <li> Record and Playback</li>
                <li> Private Roll Call</li>
                <li> Individual and Group Mute/Unmute</li>
                <li> Conference Lock</li>
                <li> Operator Assistance if needed</li>
                <li> Dial-Out to Participants</li>
            </ul>
            <p class="text-justify">
                Reservationless can be combined with web conferencing for a web and audio-integrated experience that adds impact to your message, increases conference participation and puts you in control with the click of your mouse! Built-in features like scheduling, polling and slide presentations help you manage your time as well as your conference call.
            </p>
            <br/>
            <hr/>
            <br/>
            <h2 class="text-justify" id="reservationless_plus">
                Reservationless Plus
            </h2>
            <div class="m-5">
                <p class="text-justify">
                    THTWeb's Reservationless-Plus service offers a variety of features to customize your conference, in addition to providing quick access to your call. Standard options include:  
                </p>
                <ul>
                    <li> Record and Playback
                    </li><li> Private Roll Call
                    </li><li> Individual and Group Mute/Unmute
                    </li><li> Conference Lock
                    </li><li> Operator Assistance if needed
                    </li><li> Dial-Out to Participants
                    </li>
                </ul>
                <p class="text-justify">
                    Reservationless-Plus can be combined with web conferencing for a web- and audio-integrated experience that adds impact to your message, increases conference participation and puts you in control with the click of your mouse! Built-in features like scheduling, polling and slide presentations help you manage your time as well as your conference call. 
                </p>
                <p class="text-justify">
                    <b>Reservationless-Express</b>
                    <p class="text-justify">
                        For simple access to a Reservationless conference, Reservationless-Express is your solution. It's perfect when you need to host recurring meetings, supply competitive updates, and deliver late-breaking news. Standard options include:
                    </p>
                    <ul>
                        <li>Mute/Unmute</li>
                        <li>Conference Lock</li>
                        <li>Operator Assistance</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
    <div class="section-3" id="operator_assisted">
        <div class="container">
            <a class="back-to-selection" href="#main_selection" data-scroll-location="true">Back to Top&nbsp;<i class="fas fa-arrow-up"></i></a>
            <h1 class="text-center">Operator Assisted</h1>
            <p class="text-justify">
                Enhance your call with the assistance of one of our operators and the technology of our advanced service platform. Features include:
            </p>
            <ul>
                <li>
                    <b>Leader-View</b> on-line interface to your call. Allows you to view who's on line and can be used to manage on-line questions and polling.
                </li>
                <li>
                    <b>Event Registration</b> allows you to collect information, using our operators, on your participants (Name, Email, number, etc.) as they join the call.
                </li>
                <li>
                    <b>Polling</b>  allows you to survey your participants via the use of their keypads in real time.
                </li>
                <li>
                    <b>Live Streaming</b> is available because our advanced platform is integrated into the Internet. THTWeb enables your company  to send a streaming broadcast of your event around the globe in real time.
                </li>
                <li>
                    <b>Record your call</b> for online playback and/or burn it to a CD.
                </li>
                <li>
                    <b>Transcription service</b> is available and creates a written document out of your call.
                </li>
                <li>
                    <b>With Operator Assited, there is NO limits to how many participants can join your call.</b>
                </li>
            </ul>
            <p class="text-justify">
                With a robust offering of features and enhancements, THTWeb's Operator Assisted conferencing is the best way to create the impact of an effectively coordinated event. Your conference will be more professional, interactive and engaging.   
            </p>
        </div>
    </div>
    {{-- <div class="section-4" id="manage_event">
        <div class="container">
            <a class="back-to-selection" href="#main_selection" data-scroll-location="true">Back to Top&nbsp;<i class="fas fa-arrow-up"></i></a>
            <h1 class="text-center">Manage Event / IR</h1>
            <p class="text-justify">
                Want to ensure that your event conference will start smoothly and on time? Looking for the convenience of an automated call while still getting the benefits of a full service event? 
            </p>
            <p class="text-justify">
                    Managed Event offers the customization of a large-scale event call with the convenience of automated conferencing. Participants join your conference call by entering a passcode, eliminating the need for an operator to connect them to your event. By streamlining the entry process, Managed Event reduces hold times. Plus, you can select from over 40 features to tailor each Managed Event to your business communication needs. 
            </p>
            <p class="text-justify">
                <b>When should you use Managed Event? </b>
            </p>
            <ul>
                <li> Host a company-wide quarterly review call and get everyone into the call and up to speed with no delays 
                </li><li> Use Managed Event Registration before your call and automatically collect information about your attendees as they enter the conference 
                </li><li> Streamline entry into your conference with permanent Leader and Participant Dial-In
                Numbers to begin the call quickly and efficiently 
                </li>
            </ul>
            <h2>
                Event Packages
            </h2>
            <div class="m-5">
                <p class="text-justify">
                    THTWeb's Event Packages provide you with three conferencing solutions. Our Silver, Gold and Platinum packages help you simplify the planning process - your THTWeb Event Team focuses on the demands of your event so that you can focus on the message. For more information, call (866) 633-8453. 
                </p>
                <ul>
                    <li class="row">
                        <div class="col-md-4 icon-container">
                            <img src="{{asset('/images/event_package_platinum.png')}}" alt="platinum_package">
                        </div>
                        <div class="col-md-8">
                            <h2>
                                Platinum
                            </h2>
                            <p class="text-justify">
                                As the ultimate in Event Conferencing, THTWeb's Platinum Package offers you the ability to deliver slide presentations over the web. Because pictures are worth 1,000 words, your slides will deliver complex information quickly and clearly. The Platinum Packages offers 11 additional valuable services to ensure maximum impact of your event. It is ideal for employee and medical training, product releases, and much more. 
                            </p>
                        </div> 
                    </li>
                    <li class="row">
                        <div class="col-md-4 icon-container">
                            <img src="{{asset('/images/event_package_gold.png')}}" alt="gold_package">
                        </div>
                        <div class="col-md-8">
                            <h2>
                                Gold
                            </h2>
                            <p class="text-justify">
                                THTWeb's Gold Package includes all the great audio conferencing solutions found in the Silver package, plus the power and reach of streaming, a transcription of the call for press release sound bytes, and a tape of the call for your records. When you need to broadcast your message to the world in additional to hosting a flawless event, the Gold Package is ideal. It's perfect for Investor Relations and other public events. 
                            </p>
                        </div> 
                    </li> 
                    <li class="row">
                        <div class="col-md-4 icon-container">
                            <img src="{{asset('/images/event_package_silver.png')}}" alt="silver_package">
                        </div>
                        <div class="col-md-8">
                            <h2>
                                Silver
                            </h2>
                            <p class="text-justify">
                                The Silver Package offers all the essential tools to run a high impact conference call. You have access to event tools like Leader-View to help you manage Q&A and Encore Replay for those who could not attend the call live. As with all Event Calls, your conference will be managed by experts and adhere to stringent levels of quality and service. 
                            </p>
                        </div> 
                    </li>                   
                </ul>
                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>Package Includes</th>
                            <th class="platinum-background text-center">Platinum</th>
                            <th class="gold-background text-center">Gold</th>
                            <th class="silver-background text-center">Silver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>60 min. of audio conferencing</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Leader-View</th>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Encore toll-free replay telephone access</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Encore Report of participants</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Facts Complete participant report</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Cassette Tape OR CD documentation</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>48-hr. turnaround call transcription</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Streaming Audio Live</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Streaming Audio Archive</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Streaming and Slides Live</td>
                            <td><i class="fas fa-check"></i></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Streaming and Slides Archive</td>
                            <td><i class="fas fa-check"></i></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Broadcast (email or fax)</td>
                            <td><i class="fas fa-check"></i></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>Features and Options</th>
                            <th class="platinum-background text-center">Platinum</th>
                            <th class="gold-background text-center">Gold</th>
                            <th class="silver-background text-center">Silver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Event Services Reservation Line</td>
                            <td colspan="3" class="text-center"><b>(866)633-8453</b></td>
                        </tr>
                        <tr>
                            <td>Walk-Through</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Pre-Call Subconference</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Private Communication Line</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Q & A Session</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Call Management Specialist</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                        <tr>
                            <td>Event Services Expert Operators</td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                            <td><i class="fas fa-check"></i></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <h2>
                Leader-View
            </h2>
            <div class="m-5">
                <p class="text-justify">
                    Think about your high-profile calls ... Would it simplify your business if you had the ability to actually see who is on your call and know ahead of time which guests are waiting to ask questions? How would you like to know which key participants are on the line before you start your conference? 
                </p>
                <p class="text-justify">
                    Leader-View from THTWeb allows you to do just that! Don't sit in the dark anymore – let Leader-View help you manage your audience. You will be ensured a smooth, secure meeting. 
                </p>
                <p class="text-justify">
                    <b>When should you use Leader-View?</b>
                </p>
                <div class="m-5">
                    <p class="text-justify">
                        Virtually any Event Call is more effective with Leader-View, including:
                    </p>
                    <ul>
                        <li>
                            <h3>Investor Relations Calls</h3>
                            <p class="text-justify">
                                Manage your conference attendance and Q&A session in full compliance with RegFD.
                            </p>
                        </li>
                        <li>
                            <h3>Product Launches</h3>
                            <p class="text-justify">
                                Make sure all of your participants are present before breaking the big news.
                            </p>
                        </li>
                        <li>
                            <h3>Press Conferences</h3>
                            <p class="text-justify">
                                Priorities your Q&A queue to ensure that key questions are taken first.
                            </p>
                        </li>
                        <li>
                            <h3>Distance Learning Sessions</h3>
                            <p class="text-justify">
                                Keep track of who left the call early for later follow-up on issues that were missed.
                            </p>
                        </li>
                    </ul>
                </div>
                <p class="text-justify">
                    <b>How does it work?</b>
                </p>
                <div class="m-5">
                    <p class="text-justify">
                        Leader-View's simple web-based conference interface gives you a private, real-time view of your call's participant information. Everything you need is on your screen, including Q&A Queue Control, Chat and List Downloads and much more. Respond quickly and make better decisions with a few click of your mouse.
                    </p>
                </div>
                <p class="text-justify">
                    <b>Does it require special equipment?</b>
                </p>
                <div class="m-5">
                    <p class="text-justify">
                        You only need a PC with Microsoft Explorer™ 5.0 (or higher) or Netscape Navigator/Communicator™ 6.0 (or higher).
                    </p>
                </div>
            </div>

            <h2>
                Investor Relations Call
            </h2>
            <div class="m-5">
                <p class="text-justify">
                    Imagine preparing for your quarterly Investor Relations conference call. There are people to contact, arrangements to be made, and information to be disseminated. Let Investor Relations Call help you pull it together. 
                </p>
                <p class="text-justify">
                    Investor Relations Call specializes in satisfying your specific Investor Relations conference call needs and understands the critical importance of your financial events and RegFD. Investor Relations Call directs and implements your corporate Investor Relations conferences so you can focus on the message, not the method of delivery. 
                </p>
                <p class="text-justify">
                    Your customer's expectation level for information is dramatically increasing. As a part of our Event Services Division, IRcall delivers expert service and unmatched attention to detail. Our Event Experts are trained to provide quality, consistency and complete satisfaction. 
                </p>
                <p class="text-justify">
                    <b>Enhance you Investor Relations Call</b>
                </p>
                <p class="text-justify">
                    Select from a host of conference enhancements. Our most popular features include:
                </p>
                <div class="m-5">
                    <ul>
                        <li>
                            <h3>Leader-View</h3>
                            <p class="text-justify">
                                A web-based tool that allows you to view in real-time participants who are on the call.
                                This feature provides you with the ability to screen and prioritize a question and answer session.
                            </p>
                        </li>
                        <li>
                            <h3>Streaming</h3>
                            <p class="text-justify">
                                Broadcast your investor relations call over the internet and have it archived. A link to your stream can be posted 
                                on your web site for public accesss or sent to those who have missed the call.
                            </p>
                        </li>
                        <li>
                            <h3>Call Record Option</h3>
                            <p class="text-justify">
                                Increase the reach of you conference by digitally recording your call for those who were unable to atend it live.
                                Encore is accessible via a toll or toll-free number for easy 24/7 retrieval.
                            </p>
                        </li>
                        <li>
                            <h3>Communication Line</h3>
                            <p class="text-justify">
                                The call leader's representative can communicate with the load Operator outside of the conference to give timing cues, 
                                screen participants, or manage other "behind the scenes" issues throughout the call.
                            </p>
                        </li>
                    </ul>

                </div>
                <p class="text-justify">
                    A complete range of Investor Relations Call options are available to make the most of your event.
                </p>
            </div>

        </div>
    </div> --}}
@endsection

@section('scripts')

@endsection