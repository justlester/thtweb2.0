@extends('layouts.layout')


@section('title')
News
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/news_style.css')}}">
@endsection

@section('content')
    <div class="container">
        <div class="main-card card">
            <div class="card-body">
                <h1>News</h1>
                <div class="row">
                    <div class="col-md-6 news-item-container">
                        <a class="news-item card" href="#">
                            <div class="overlay-hover">
                                <div class="inner-overlay-hover">
                                    <div class="view-icon-cirlce">
                                        <i class="far fa-eye"></i><br/>
                                        View
                                    </div>
                                </div>
                            </div>
                            <div class="news-img" style="background-image:url('{{asset('/images/news_default.png')}}');"></div>
                            <div class="card-body">
                                <h5 class="card-title">THTWeb, thtweb worldwide, introduces one touch recording service for recording web conferencing</h5>
                                <p class="card-text">June 2005</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 news-item-container">
                        <a class="news-item card" href="#">
                            <div class="overlay-hover">
                                <div class="inner-overlay-hover">
                                    <div class="view-icon-cirlce">
                                        <i class="far fa-eye"></i><br/>
                                        View
                                    </div>
                                </div>
                            </div>
                            <div class="news-img" style="background-image:url('{{asset('/images/news_default.png')}}');"></div>
                            <div class="card-body">
                                <h5 class="card-title">THTWeb, thtweb worldwide launches secureconnect for secure audio conferencing</h5>
                                <p class="card-text">May 2005</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 news-item-container">
                        <a class="news-item card" href="#">
                            <div class="overlay-hover">
                                <div class="inner-overlay-hover">
                                    <div class="view-icon-cirlce">
                                        <i class="far fa-eye"></i><br/>
                                        View
                                    </div>
                                </div>
                            </div>
                            <div class="news-img" style="background-image:url('{{asset('/images/news_default.png')}}');"></div>
                            <div class="card-body">
                                <h5 class="card-title">THTWeb, thtweb worldwide, adds india to its international toll free service (itfs)</h5>
                                <p class="card-text"> April 2005</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 news-item-container">
                        <a class="news-item card" href="#">
                           <div class="overlay-hover">
                                <div class="inner-overlay-hover">
                                    <div class="view-icon-cirlce">
                                        <i class="far fa-eye"></i><br/>
                                        View
                                    </div>
                                </div>
                            </div>
                            <div class="news-img" style="background-image:url('{{asset('/images/news_default.png')}}');"></div>
                            <div class="card-body">
                                <h5 class="card-title">thtweb worldwide signs 1,000th customer</h5>
                                <p class="card-text">  March 2005</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 news-item-container">
                        <a class="news-item card" href="#">
                            <div class="overlay-hover">
                                <div class="inner-overlay-hover">
                                    <div class="view-icon-cirlce">
                                        <i class="far fa-eye"></i><br/>
                                        View
                                    </div>
                                </div>
                            </div>
                            <div class="news-img" style="background-image:url('{{asset('/images/news_default.png')}}');"></div>
                            <div class="card-body">
                                <h5 class="card-title">thtweb worldwide names jeff lutton vice president of sales and partner</h5>
                                <p class="card-text">  February 2005</p>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection