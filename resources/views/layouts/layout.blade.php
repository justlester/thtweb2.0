<!DOCTYPE html>
    <head>
    <link rel="icon" href="{{asset('images/thtweb_icon_logo.png')}}">
        <title>THTWeb | @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="justlester">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('styles')
    </head>
    <body>
        @include('navs.nav')
        <div class="content">
                @yield('content')
        </div>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 address-contact">
          
                        <div class="inner-address-contact">
                            <h1>THTWeb</h1>
                            <p>
                                8230 Old Courthouse Road, Suite 460<br/>
                                Vienna, VA 22182<br/>
                            </p>
                            <p>
                                Toll Free:	(866) 633-8453<br/>
                                Main:	(703) 752-1200<br/>
                                FAX:	(703) 752-7454<br/>
                                Email:	<a href="mailto:info@thtweb.com">info@thtweb.com</a><br/>    
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="text-justify">
                            THTWeb Worldwide (THTWeb), provides audio conferencing, web conferencing and webinar solutions to business customers. THTWeb web Conferencing services enable live meetings online that are simple and easy to use for customers that need a reliable web conferencing solution. Our International audio conferencing and international web conferencing solutions provide organizations a more efficient way to meet, train, and sell to remote individuals and organizations.
                        </p>
                        <p class="text-center">
                            <a href="#" data-toggle="modal" data-target="#privacy_policy_modal">Privacy Policy</a> | <a href="#"  data-toggle="modal" data-target="#service_agrmnt_modal">Service Agreement</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <p class="text-center footer-copyright">
                    &copy; 2018 All Rights Reserved | Developed by <a href="https://www.archintel.com">Archintel Corp</a>.
                </p>
            </div>
        </div>
        @include('modals.sign-up')
        @include('modals.privacy-policy')
        @include('modals.service-agrmnt')
    </body>
    <script src="{{mix('js/app.js')}}"></script>
</html>