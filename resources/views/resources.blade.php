@extends('layouts.layout')


@section('title')
Resources
@endsection

@section('styles')
    <link rel="stylesheet" href="{{mix('css/resources_style.css')}}">
@endsection

@section('content')
<div class="section-1">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h1 class="text-center">Resources</h1>
                <h2 class="text-left">Useful Links</h2>
                <ul>
                    <li>
                        <a href="http://www.businessranks.com" target="_blank">Business Ranks</a>
                    </li>
                    <li>
                        <a href="http://www.clariscapital.com" target="_blank">Claris Capital</a>
                        Claris Capital LLC is a private investment bank based in the Washington, D.C. area which 
                        provides expert mergers and acquisitions, financial restructuring and capital formation 
                        advisory services to middle-market companies.  
                    </li>
                    <li>
                        <a href="http://networking.ITtoolbox.com" target="_blank">ITtoolbox Networking Knowledge Base</a> 
                        Content, community, and service for Networking professionals.
                        Providing technical discussion, job postings, an integrated directory, news,
                        and much more.  
                    </li>
                    <li>
                        <a href="http://www.linkpartners.com/index.html" target="_blank">LinkPartners</a>  
                        The easy way to find link swap partners.  
                    </li>
                    <li>
                        <a href="http://www.nvtc.org" target="_blank">Northern Virginia Technology Council</a> 
                        The Northern Virginia Technology Council is the membership association for the technology 
                        community in Northern Virginia. NVTC has more than 1,300 member companies representing 
                        over 170,000 employees.  
                    </li>
                    <li>
                        <a href="http://www.polycom.com" target="_blank">Polycom</a> 
                        Polycom develops, manufactures and markets a full range of high-quality, easy-to-use 
                        and affordable voice and video communication endpoints, video management software, 
                        web conferencing software, multi-network gateways, and multipoint conferencing 
                        and network access solutions. 
                    </li>
                    <li>
                        <a href="http://www.potomacofficersclub.com" target="_blank">Potomac Officers Club</a>
                        The POC provides a fun and thought-provoking monthly forum for Washington, DC area 
                        executives to remain connected, in turn complementing the area's continued economic 
                        growth and development for large, established firms, emerging growth organizations, 
                        and executives alike.     
                    </li>
                    <li>
                        <a href="http://www.salesrobot.com" target="_blank">Sales Robot</a> 
                        Automate your salesforce with SalesRobot.   
                    </li>
                    <li>
                        <a href="http://www.tenav.co.uk" target="_blank">TENav</a> 
                        TEN Audio Visual provides both Video &amp; Audio Conferencing Solutions. Services include 
                        specification, consultancy, equipment supply, installation &amp; integration, support &amp; 
                        maintenance and training.  
                    </li>
                    <li>
                        <a href="http://www.tandbergusa.com" target="_blank">Tandberg</a> 
                        A leading global provider of collaborative communication solutions. The company designs, 
                        develops and manufactures videoconferencing systems and management software. 
                    </li>
                    <li>
                        <a href="http://www.telcoa.org" target="_blank">The Telework Coalition</a>  
                        Dedicated to the advancement of telework and telecommuting and those who support us. 
                    </li>
                    <li>
                        <a href="http://thinkofit.com/webconf/" target="_blank">Think of It Conferencing on the Web</a> 
                        A comprehensive guide to software that powers discussions on the internet.  
                    </li>
                    <li>
                        <a href="http://www.theukwebdesigncompany.com" target="_blank">UK Web Design</a> 
                        We have thousands of web design companies registered in our database. Request a quote today and 
                        we will do all the hard work of finding you the best quotations from our huge range of web designers.  
                    </li>
                    <li>
                        <a href="http://www.viget.com" target="_blank">Viget Labs</a> 
                        A leading web systems consulting and development firm based just outside of Washington, DC. 
                        Viget provides strategy, design, and development services to clients across the country.   
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section-2">
    <div class="container">
        <h2>White Papers</h2>
        <p class="text-center">
            These white papers are intended to help educate telcom shoppers to make smart choices regarding teir teleconferencing needs.
        </p>
        <div class="row">
            <div class="col-md-6">
                <a class="white-paper-btn" href="{{asset('files/pdfs/THT-Conference_Decision_White_Paper.pdf')}}" target="_blank">
                    <div class="icon">
                        <div class="inner-icon">
                            <i class="fas fa-file-pdf"></i>
                        </div>
                    </div>
                    <div class="filename">
                        <div class="inner-filename">
                            Match the right web conferencing solution with your needs
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a class="white-paper-btn" href="{{asset('files/pdfs/THT-Conference_Bridges_White_Paper.pdf')}}" target="_blank">
                    <div class="icon">
                        <div class="inner-icon">
                            <i class="fas fa-file-pdf"></i>
                        </div>
                    </div>
                    <div class="filename">
                        <div class="inner-filename">
                            Conference Bridges
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a class="white-paper-btn" href="{{asset('files/pdfs/THT-Conference_ask_your_champion.pdf')}}" target="_blank">
                    <div class="icon">
                        <div class="inner-icon">
                            <i class="fas fa-file-pdf"></i>
                        </div>
                    </div>
                    <div class="filename">
                        <div class="inner-filename">
                            Web Conferencing: Ask Your Champion
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a class="white-paper-btn" href="{{asset('files/pdfs/GOING_GLOBAL_WITH_THTWEB.pdf')}}" target="_blank">
                    <div class="icon">
                        <div class="inner-icon">
                            <i class="fas fa-file-pdf"></i>
                        </div>
                    </div>
                    <div class="filename">
                        <div class="inner-filename">
                            Going Global with THTWeb
                        </div>
                    </div>
                </a>  
            </div>
            <div class="col-md-6">
                <a class="white-paper-btn" href="{{asset('files/pdfs/FCEDA_Article_Web_Conferencing.pdf')}}" target="_blank">
                    <div class="icon">
                        <div class="inner-icon">
                            <i class="fas fa-file-pdf"></i>
                        </div>
                    </div>
                    <div class="filename">
                        <div class="inner-filename">
                            Web Conferencing: Why all the Fuss?
                        </div>
                    </div>
                </a>  
            </div>
        </div>
        <br/>
        <br/>
        <h2>Related Articles and Industry</h2>
        <ul>
            <li>
                <a href="http://www.nwfusion.com/columnists/2004/092704bradner.html" target="_blank">"Tattletale WebEx"</a>
                by Scott Bradner Network World, September 27, 2004 
            </li>
            <li>
                <a href="http://www.intelligententerprise.com/showArticle.jhtml?articleID=51201372" target="_blank"> R U Compliant?</a>
                by Stewart McKie, November 13, 2004 
            </li>
            <li>
                <a href="http://www.cxotoday.com/cxo/jsp/index.jsp?section=News&amp;subsection=Business&amp;subsection_code=1&amp;file=template1.jsp&amp;storyid=1649" target="_blank">"Collaboration Guns For Web Conferencing Pie"</a>
                by CXOtoday Staff Mumbai, October 27, 2004
            </li>
        </ul>
        <br/>
        <br/>
        <h2>Video Conferencing</h2>
        <p class="text-justify">
            THTWeb has partnered with Video Solutions Group VSGI to provide the best in video conferencing. Visit VSGI's web site to learn more about our video conferencing services.
        </p>
        <a class="visit-vsgi" href="http://www.vsgi.com" target="_blank">Visit VSGI</a>
    </div>
</div>
<div class="section-3">
    <div class="container">
        <h2>Webinars</h2>
        <p class="text-justify">
            THTWeb is pleased to present the following upcoming webinars:
        </p>
        <div class="m-3">
            <ul>
                <li>
                    <h1 class="webinars-eventname"><b>Fridays, 12pm EDT</b> - "Getting Started with Online Events"</h1>
                    <p class="text-justify">
                        We specialize in conference calls, audio Conferences, web conferencing and customized multi-person communications.
                    </p>
                    <ul>
                        <li>Reservationless Conference Calls</li>
                        <li>Operator Assisted</li>
                        <li>Web Conferencing</li>
                        <li>Microsoft Outlook Integration</li>
                        <li>Web and/or audio from around the world and pay in US Dollars</li>
                        <li>International toll-free origination from 50 Countries</li>
                    </ul>
                </li>
            </ul>
        </div>
        <p class="text-justify">
            Previous Webinars Hosted by THTWeb:
        </p>
        <div class="m-3">
            <ul>
                <li>
                    <h1 class="webinars-eventname"><b>June 14, 2005</b> - "Fill Your Sales Pipeline with the Million-Dollar Web Seminar Presentation"</h1>
                    <p>
                        <b>Featuring Ira Koretsky</b><br/>
                        Chief Storyteller, Koretsky Communications Group 
                    </p>

                    <p class="text-justify">
                        Imagine reaching several hundred qualified prospects without the time and expense of visiting each one personally. Imagine them contacting you after the presentation to set up the next meeting. Welcome to the power of web seminar presentations! 
                    </p>

                    <p class="text-justify">
                        Web seminars are fast becoming an integral tool for many sales and marketing groups. Unfortunately, only a fraction of them are creating a wow experience for their online attendees. In face-to-face presentations, you have the luxury of a few minutes to inspire and engage your audience. With web-based presentations, you only have a few moments to grab your audience’s attention and keep it. To grab them from the start, you must deliver a wow experience. A wow experience begins with an inspiring business story and message, compelling visuals, and confident delivery. All must be synched and executed flawlessly. 
                    </p>

                    <p class="text-justify">
                        Spend an hour with us and learn how to create a wow seminar presentation for your audiences. 
                    </p>

                    <p class="text-justify">
                        If you are looking to fill your sales pipeline, join us as we cover these key topics:
                    </p>

                    <ul>
                            <li>Avoid the blah-blah-blah syndrome using audience analysis techniques
                            </li><li>8 steps to reduce substantially the time to develop an inspiring presentation
                            </li><li>Establish and maintain rapport without face-to-face contact
                            </li><li>Emphasize points without gestures or facial expressions
                            </li><li>Use the strengths of the online medium to credibly persuade and inspire attendees
                            </li>
                    </ul>

                    <p class="text-justify">
                        Join us for this highly interactive seminar where you will learn how to bring your visual story alive and deliver your story so that it sets you apart. 
                    </p>

                    <p class="text-justify">
                        <b>Who Should Attend? </b>
                    </p>
                    <div class="m-3">
                        <p class="text-justify">
                            Directors, Vice Presidents, and CxOs, responsible for sales, development, marketing, and lead generation in their organization. 
                        </p>
                    </div>

                    <p class="text-justify">
                        <b>Free Evaluation of Your PowerPoint Presentation:</b>
                    </p>

                    <div class="m-3">
                        <p class="text-justify">
                            All attendees are eligible for a free Story Diagnosis of their presentation (instructions are provided during the web seminar). 
                        </p>
                    </div>

                    <p class="text-justify">
                        <b>About Koretsky Communications Group and the Presenter, Ira Koretsky: </b>
                    </p>
                    <div class="row">
                        <div class="col-md-3">
                            <img class="img-fluid presenter-photo" src="{{url('/images/ira_koretsky_photo.jpg')}}" alt="ira_koretsky_photo">
                        </div>
                        <div class="col-md-9">
                            <p class="text-justify">
                                We are business storytellers. We help you tell your story so that it resonates with audiences, from start to finish. As the Chief Storyteller for KCG, Koretsky created the Power Story Program to close the communication gap between sales, marketing, technical services, and the client. The services are proven to help your organization translate your vision into your new business story—a story that resonates with audiences—a story that is compelling, credible, consistent, engaging, and inspiring. KCG has told the business stories of not-for-profit, government, educational, emerging, and Fortune 500 organizations such as Charles Schwab, Warner Bros., Morgan Stanley, Smith School of Business at the University of Maryland, and the University of California, Berkeley. 
                            </p>
                            <p class="text-justify">
                                Koretsky is a leading authority on business communications and web seminar presentation design and delivery. He is a professional speaker, idea-generator, creativity guy, and improv comedy performer with ComedySportz. Ira is also an event speaker and workshop presenter across the country.
                            </p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection